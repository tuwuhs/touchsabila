/* main.qml
 * TouchSabila
 *
 * (c) 2013 Tuwuh Sarwoprasojo, Labtek Indie
 * All rights reserved.
 */

import QtQuick 2.0

Rectangle {
    width: 800
    height: 600

    Text {
        text: sequencer.step
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }
    }

    Grid {
        anchors.centerIn: parent
        columns: 16; rows: 12
        columnSpacing: 20; rowSpacing: 20
        Repeater {
            model: parent.rows*parent.columns
            Rectangle {
                id: pad
                property int row: (index % 16)
                property int column: Math.floor(index / 16)
                property bool triggered: (row === sequencer.step)
                property bool active: false

                width: 30; height: 30; radius: 15
                border {
                    color: triggered? "blue": "black"
                    width: triggered? 2: 1
                }
                color: active? "#37c5f3": "white"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (pad.active) {
                            pad.active = false;
                        } else {
                            pad.active = true;
                        }
                        sequencer.setPad(pad.row, pad.column, pad.active);
                    }
                }

                Rectangle {
                    id: trigFx
                    anchors.fill: parent
                    radius: width/2
                    border {
                        color: Qt.darker("#37c5f3", 1.5)
                        width: 3
                    }
                    color: "transparent"
                    opacity: 0
                    ParallelAnimation {
                        id: trigFxAnimation
                        running: false
                        PropertyAnimation {
                            target: trigFx
                            property: "scale"
                            from: 1.0
                            to: 3.0
                            duration: 700
                        }
                        PropertyAnimation {
                            target: trigFx
                            property: "opacity"
                            from: 0.5
                            to: 0
                            duration: 700
                        }
                    }
                }

                Behavior on color {ColorAnimation {duration: 100}}
                onTriggeredChanged: {
                    if (triggered && active) {
                        trigFxAnimation.running = true;
                    }
                }
            }
        }
    }
}
